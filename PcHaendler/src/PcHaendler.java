import java.util.Scanner;

public class PcHaendler {
	
	
	public static String liesString(String text){
		Scanner myScanner= new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
	}
	
	public static int liesInt(String text){
		Scanner myScanner= new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(String text) {
		Scanner myScanner= new Scanner(System.in);
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();
		return preis;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl*nettopreis;
	}
	
	public static double berechneGesamtBruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst);
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.2f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	 
	
	
	public static void main(String[] args) {
		
		
		String meinArtikel= liesString("was m�chten Sie bestellen?");
		int AnzahlArtikel = liesInt("Geben Sie die Anzahl ein:");
		double NettoPreis= liesDouble("Geben Sie den Nettopreis ein:");
		Scanner myScanner= new Scanner(System.in);
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble()/100;
		double Netto = berechneGesamtnettopreis(AnzahlArtikel, NettoPreis);
		double Brutto= berechneGesamtBruttopreis(Netto, mwst);
		rechungausgeben(meinArtikel, AnzahlArtikel, Netto, Brutto, mwst);
		
		
		
		// Benutzereingaben lesen
		/*System.out.println("was m�chten Sie bestellen?");
		String artikel = myScanner.next();*/

		/*System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();*/

		/*System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();*/

		/*System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();*/

		// Verarbeiten
		/*double nettogesamtpreis = anzahl * preis;*/
		/*double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);*/

		// Ausgeben

		/*System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		*/
	}
}