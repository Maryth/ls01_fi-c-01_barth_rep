﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
	public static double fahrkartenbestellungErfassen(){
		double gesamtbetrag = 0;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("=========================");
		System.out.println("Fahrkartenbestellvorgang:\n"
				+ "=========================");
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
				+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
				+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
				+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n"
				+ "");
		System.out.println("Bitte geben Sie eine Zahl (1-3) ein:");
		byte tickettyp = tastatur.nextByte();
		double preis =0;
		boolean eingabegueltig = false;
		
		while (eingabegueltig == false) 
		{
			if (tickettyp == 1) 
			{
				preis = 2.90;
				System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] ausgewählt.");
				eingabegueltig = true;
			}
			else if ( tickettyp == 2)
			{
				preis = 8.60;
				System.out.println("Tageskarte Regeltarif AB [8,60 EUR] ausgewählt.");
				eingabegueltig = true;
			}
			else if (tickettyp == 3)
			{
				preis = 23.50;
				System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] ausgewählt");
				eingabegueltig = true;
			}
			else
			{
				System.out.println("Ungültige Eingabe.Bitte machen Sie eine gültige Eingabe:");
				byte neuticket = tastatur.nextByte();
				tickettyp = neuticket;
			}
		}	
		
        System.out.print("Anzahl der Tickets: ");
        int anzahl = tastatur.nextInt();
        
        if(anzahl > 0 && anzahl <=10) 
        {
        	System.out.println("Es wurden "+ anzahl + " Ticket(s) ausgewählt.");
        }
        else 
        {
        	while(anzahl < 1 || anzahl > 10) 
        	{
        	System.out.println("Bitte machen Sie eine gültige Eingabe:");
        	 int neuanzahl = tastatur.nextInt();
        	 anzahl = neuanzahl;
        	}
        }	
        gesamtbetrag = preis * anzahl;
        return gesamtbetrag;
 
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
           while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
           {
               System.out.printf("Noch zu zahlen: %.2f Euro\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
               System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
               double eingeworfeneMünze = tastatur.nextDouble();
               eingezahlterGesamtbetrag += eingeworfeneMünze;
           }
           return eingezahlterGesamtbetrag;
		
	}
	public static void fahrkartenAusgeben() {
	System.out.println("\nFahrschein wird ausgegeben");
	      for (int i = 0; i < 8; i++)
	      {
	         System.out.print("=");
	         warten(100);
	      }
	    System.out.println("\n\n");	
		
	}
	
	public static void warten(int milliesekunde) {
		try {
			Thread.sleep(milliesekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag +" "+ einheit);
		
	}
	
	public static void rueckgeldAusgeben(double eingezahlterBetrag, double gesamtbetrag) {
		double rückgabebetrag = eingezahlterBetrag - gesamtbetrag;
	    if(rückgabebetrag > 0.0)
	       {
	    	System.out.printf("Der Rückgabebetrag in Höhe von %.2f%s%n " ,  rückgabebetrag , " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  muenzeAusgeben(2, "EURO");
	        	   //System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	   muenzeAusgeben(1, "EURO");
	        	  //System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	   muenzeAusgeben(50, "CENT");
	        	  //System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	   muenzeAusgeben(20, "CENT");
	        	  //System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	   muenzeAusgeben(10, "CENT");
	        	  //System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	   muenzeAusgeben(5, "CENT");
	        	  //System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }	
	    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
        "vor Fahrtantritt entwerten zu lassen!\n"+
        "Wir wünschen Ihnen eine gute Fahrt."+
        "\n");
	}
	
	
    public static void main(String[] args)
    {
    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    	
    	System.out.println("Kauf abgeschlossen. Ticketkauf wird nun fortgesetzt:");
    	boolean repeat = true;
    	
    	while( repeat = true) 
    	{
    		zuZahlenderBetrag = fahrkartenbestellungErfassen();
        	eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        	fahrkartenAusgeben();
        	rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
        	
        	System.out.println("Kauf abgeschlossen. Ticketkauf wird nun fortgesetzt:");
    		
    	}
    	
   }
    	
}