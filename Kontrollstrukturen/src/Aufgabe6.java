import java.util.Scanner;

public class Aufgabe6 
{
	public static void main (String[]args)
	{
		Scanner eingabe = new Scanner(System.in);
		System.out.println("Willkommen beim BMI-Rechner!");
		System.out.println("Bitte gebe dein Geschlecht an (m/w):");
		char geschlecht = eingabe.next().charAt(0);
		System.out.println("Bitte gebe dein Gewicht in kg an:");
		int gewicht = eingabe.nextInt();
		System.out.println("Bitte gebe deine K�rpergr��e in cm an:");
		double groesse = eingabe.nextDouble();
		double cmgroesse= groesse/100;
				
		double bmi = gewicht/(cmgroesse*cmgroesse);
		
		if(geschlecht == 'w') 
		{
			if(bmi < 19)System.out.printf("Dein BMI betr�gt %.1f und du hast somit Untergewicht.", bmi);
		
			else if(bmi <= 24)System.out.printf("Dein BMI betr�gt %.1f und du hast somit Normalgewicht.", bmi);
		
			else System.out.printf("Dein BMI betr�gt %.1f und du hast somit �bergewicht.", bmi);
		}
		else if(geschlecht == 'm')
		{
			if(bmi < 20)System.out.printf("Dein BMI betr�gt %.1f und du hast somit Untergewicht.", bmi);
			
			else if(bmi <= 25)System.out.printf("Dein BMI betr�gt %.1f und du hast somit Normalgewicht.", bmi);
			
			else System.out.printf("Dein BMI betr�gt %.1f und du hast somit �bergewicht.", bmi);
		}
		else
		{
			System.out.printf("Bitte mach eine g�ltige Eingabe.");
		}
	}
	
	
	
	
	
	
	
}
