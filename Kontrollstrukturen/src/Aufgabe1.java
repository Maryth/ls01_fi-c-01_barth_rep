import java.util.Scanner;

public class Aufgabe1 {

	//Aufgabe 1
	
	public static void Noten () {
		Scanner myScanner= new Scanner(System.in);
		System.out.println("Geben Sie eine Note an:");
		int ziffer= myScanner.nextInt();
		
		if(ziffer==1) {
			System.out.println("Die sprachliche Umschreibung f�r diese Ziffer lautet: Sehr gut.");
		}
		else if(ziffer==2) {
			System.out.println("Die sprachliche Umschreibung f�r diese Ziffer lautet: Gut.");
		}
		else if(ziffer==3) {
			System.out.println("Die sprachliche Umschreibung f�r diese Ziffer lautet: Befriedigend.");
		}
		else if(ziffer==4) {
			System.out.println("Die sprachliche Umschreibung f�r diese Ziffer lautet: Ausreichend.");
		}
		else if(ziffer==5) {
			System.out.println("Die sprachliche Umschreibung f�r diese Ziffer lautet: Mangelhaft.");
		}
		else if(ziffer==6) {
			System.out.println("Die sprachliche Umschreibung f�r diese Ziffer lautet: Ungen�gend.");
		}
		else {
			System.out.println("Diese Eingabe ist ung�ltig.");
		}
	}
	
	
	public static void main (String[]args) {
		
		Noten(); //Aufgabe 1
		
		
	}
	
}
