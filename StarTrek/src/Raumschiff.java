import java.awt.datatransfer.SystemFlavorMap;
import java.awt.print.Printable;
import java.util.ArrayList;

/**
 * 
 * @author Maria Barth
 * @version 1.5
 * @
 *
 */


public class Raumschiff 
{
	
	/**
	 *----------------------------------------------------------------------ATTRIBUTE----------------------------------------------------------------------
	 */
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKummonikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	/**
	 *----------------------------------------------------------------------KONSTRUKTOREN----------------------------------------------------------------------
	 *leerer Konstruktor 
	 */
	
	public Raumschiff()
	{
		
	}

	/**
	 * 
	 * @param photonentorpedoAnzahl gibt an wie viele Photonentorpedos an Bord des Raumschiffes sind
	 * @param energieversorgungInProzent gibt an wie hoch die Energieversorgen in Prozent ist
	 * @param schildeInProzent gibt an wie der Zustand der Schilde in Prozent ist
	 * @param huelleInProzent gibt an wie der Zustand der H�lle in Prozent ist
	 * @param lebenserhaltungssystemInProzent gibt an wie der Zustand der Lebenerhaltungssysteme in Prozent ist
	 * @param androidenAnzahl gibt an wie viele Reperaturandroiden sich auf dem Raumschiff befinden
	 * @param schiffsname gibt den Namen des Raumschiffes an
	 */
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemInProzent, int androidenAnzahl, String schiffsname) 
	{	
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;	
	}
	
	/**
	 *----------------------------------------------------------------------GETTER UND SETTER---------------------------------------------------------------------- 
	 */
	
	
	/**
	 *Photonentorpedoanzahl _____________________________________
	 * Setzt die Anzahl der Photonentorpedos
	 * @param photonentorpedo setzt die Anzahl der Photnentorpedos
	 */
	
	public void setPhotonentorpedoAnzahl ( int photonentorpedo)  
	{
		this.photonentorpedoAnzahl = photonentorpedo;	
	}
	
	/**
	 * Liefert die Anzahl der Photonentorpddos
	 * @return gibt Anzahl der Photonentorpedos als Datentyp int wieder
	 */
	
	public int getPhotonentorpedoAnzahl () 
	{
		return photonentorpedoAnzahl;
	}
	
	/**
	 * Energieversorgung in Prozent _____________________________________
	 * Setzt die H�he der Energieversorgung des Raumschiffes in Prozent
	 * @param energieversorgung setzt die Prozente der Ernergieversorgung
	 */
	
	public void setEnergieversorgungInProzent ( int energieversorgung)  
	{
		this.energieversorgungInProzent = energieversorgung;	
	}
	
	/**
	 * Liefert die die H�he der Lebenserhaltung in Prozent
	 * @return gibt die H�he der Lebenserhaltung in Prozent als Datentyp int wieder
	 */
	
	public int getEnergieversorgungInProzent () 
	{
		return energieversorgungInProzent;
	}
	
	/**
	 * Schild in Prozent _____________________________________
	 * Setzt den Zustand der Schilde des Raumschiffes in Prozent
	 * @param schild setzt die Prozente der Schilde
	 */
	
	public void setSchildeInProzent ( int schild)  
	{
		this.schildeInProzent = schild;
	}
	
	/**
	 * Liefert den Zustand der Schilde in Prozent
	 * @return gibt den Zustand der Schilde in Prozent als Datentyp int wieder
	 */
	
	public int getSchildeInProzent()
	{
	return schildeInProzent;
	}
	
	/**
	 *H�lle in Prozent _____________________________________
	 *Setzt den Zustand der H�lle in Prozent
	 * @param huelle setzt die Prozente der H�lle
	 */
	
	public void setHuelleInProzent (int huelle)
	{
		this.huelleInProzent = huelle;
	}
	
	/**
	 * Liefert den Zustand der H�lle in Prozent
	 * @return gibt den Zustand der H�lle in Prozent als  Datentyp int wieder
	 */
	
	public int getHuelleInProzent()
	{
		return huelleInProzent;
	}
	
	/**
	 *Lebenserhaltungssysteme in Prozent _____________________________________
	 *Setzt den Zustand der Lebenserhaltungssysteme in Prozent
	 * @param lebenserhaltungssystem setzt die Prozente der Lebenserhaltungssysteme
	 */
	
	public void setLebenserhaltungssystemeInProzent (int lebenserhaltungssystem)
	{
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystem;
	}
	
	/**
	 *Liefert den Zustand der Lebenserhaltungsysyteme in Prozent
	 * @return gibt den Zustande der Lebenserhaltungssystem in Prozent als Datentyp int wieder
	 */
	
	public int getLebenserhaltungssystemeInProzent ()
	{
		return lebenserhaltungssystemInProzent;
	}
	
	/**Androidenanzahl _____________________________________
	 * Setzt die Anzahl der Androiden 
	 * @param androiden setzt die Anzahl der Androiden
	 */
	
	public void setAndroidenAnzahl (int androiden) 
	{
		this.androidenAnzahl = androiden;
	}
	
	/**
	 * Liefert die Anzahl der Androiden
	 * @return gibt die Anzahl der Androiden als Datentyp int wieder
	 */
	
	public int getAndroidenAnzahl ()
	{
		return androidenAnzahl;
	}
	
	/**
	 * Schiffsname _____________________________________
	 * Setzt den Schiffsname des Schiffes
	 * @param schiffsname setzt den Namen des Schiffes
	 */

	public void setSchiffsname (String schiffsname)
	{
		this.schiffsname = schiffsname;
	}
	
	/**
	 * Liefert den Schiffsnamen des Schiffes
	 * @return gibt den Schiffsnamen als Datentyp String wieder
	 */
	
	public String getSchiffsname () 
	{
		return schiffsname;
	}
	
	/**
	 *----------------------------------------------------------------------METHODEN---------------------------------------------------------------------- 
	 */
	
	/**
	*Methode, um Ladung hinzuzuf�gen
	*bestehende Ladungen k�nnen einem beliebigen Raumschiff hinzugef�gt werden
	 * @param neueLadung bestimmt welche vorhandene Ladung auf ein beliebiges Raumschiff geladen werden soll
	 */
	
	public void addLadung (Ladung neueLadung) 
	{
		ladungsverzeichnis.add(neueLadung);
		
	}
	
	/**
	 *Methode, um Zustand eines Raumschiffes mit allen Attributen ausgeben
	 */
	
	public void zustandRaumschiff() 
	{
	System.out.println("Der aktuelle Status des des Raumschiffes:");
	System.out.println("Name des Raumschiffes: " + getSchiffsname());
	System.out.println("Status der Schilde: " + getSchildeInProzent() + "%");
	System.out.println("Status der H�lle: " + getHuelleInProzent() + "%");
	System.out.println("Status des Lebenserhaltungssystems: " + getLebenserhaltungssystemeInProzent() + "%");
	System.out.println("Status der Energieversorgung: " + getEnergieversorgungInProzent() + "%");
	System.out.println("Anzahl der Photonenentorpedos: " + getPhotonentorpedoAnzahl() + " St�ck");
	System.out.println("Anzahl der Reperatur-Androiden: " + getAndroidenAnzahl() + " St�ck");
	System.out.println("--------------------------------");
	}
	
	/**
	*Methode, die das Ladungsverzeichnis eines Raumschiffes ausgibt
	*gibt die einzelnen Ladungen mit Bzeichnung und Menge aus
	 */
	
	public void aktuelleLadung () 
	{
		System.out.println("Die aktuelle Ladung von " + schiffsname + ": ");
		for (Ladung ladung : ladungsverzeichnis) 
		{
            System.out.println(ladung);
        }
		System.out.println("--------------------------------");
	}
	
	/**
	 *Methode, um Photonentorpedos abzufeuern
	*ein beliebiges Raumschiff kann ein anderes beliebiges Raumschiff mit Photonentorpedos beschie�en
	*es wird ein Photonentorpedo verbraucht pro Schuss 
	*wenn der Photonentorpedo trifft gibt es die Nachricht an alle "Photonentorpedo abgeschossen" aus
	*wenn nicht genug Photonentorpedos geladen sind, dann gibt es "-=*Click*=-" als Nachricht an alle aus
	* @param zutreffendesRaumschiff gibt an welches Schiff beschossen werden soll
	 */
	
	public void photonentorpedoSchiessen (Raumschiff zutreffendesRaumschiff)
	{
		if( photonentorpedoAnzahl > 0)
		{
			photonentorpedoAnzahl--;
			nachrichtAnAlle(" Photonentorpedo abgeschossen.");
			System.out.println("Aktueller Status Photonentorpedo: " +photonentorpedoAnzahl + " St�ck.");
			treffer(zutreffendesRaumschiff);
			System.out.println("--------------------------------");
			
		}
		else 
		{
			nachrichtAnAlle("-=*Click*=-");
			System.out.println("--------------------------------");
		}
	}
	
	/**
	 *Methode, um Phaserkanone abzufeuern
	 *ein beliebiges Raumschiff kann ein anderes beliebiges Raumschiff mit Phaserkanone beschie�en
	 * eine Phaserkanone verbraucht 50% der Energieversorgung pro Schuss 
	 * wenn die Phaserkanone trifft gibt das Raumschiff die Nachricht an alle "Phaserkanone abgeschossen" aus
	 *wenn nicht genug Energieversorgung verf�gbar ist, dann gibt es "-=*Click*=-" an alle aus
	 * @param zutreffendesRaumschiff gibt an welches Schiff beschossen wird
	 */
	
		public void phaserKanoneSchiessen (Raumschiff zutreffendesRaumschiff)
		{
			if( energieversorgungInProzent >= 50)
			{
				energieversorgungInProzent = energieversorgungInProzent - 50;
				nachrichtAnAlle("Phaserkanone abgeschossen.");
				System.out.println("Energieverbrauch durch Phaserkanone betr�gt 50%.");
				System.out.println("Aktueller Status der Energieversorgung: " + energieversorgungInProzent + "%.");
				treffer(zutreffendesRaumschiff);
				System.out.println("--------------------------------");
			}
			else 
			{
				nachrichtAnAlle("-=*Click*=-");
				System.out.println("Energieversorgung zu niedrig.");
				System.out.println("--------------------------------");
			}
		}
		
	/**
	 *Methode, die einen Treffer am beschossenen Raumschiff vermerkt 
	 * @param getroffenesRaumschiff gibt an welches Raumschiff in der Methode phaserkanone bzw photonentorpedoSchiessen getroffen wurde
	 * wenn noch �ber 0% der Schilde verf�gbar sind dann wird 50% von den Schilden abgezogen
	 * wenn Schilde unter 0% sind, 50% von H�lle von Energieversorgung abgezogen
	 * wenn die H�lle unter 0% f�llt, dann wird das Schiff zerst�rt und gibt eine Nachricht an alle aus, dass es zerst�rt wurde
	 */
	
	private void treffer(Raumschiff getroffenesRaumschiff) 
	{
		System.out.println(getroffenesRaumschiff.schiffsname + "wurde getroffen!");
		
		if(getroffenesRaumschiff.schildeInProzent > 0)
		{
			getroffenesRaumschiff.schildeInProzent = getroffenesRaumschiff.schildeInProzent - 50;
		}
		else
		{
			getroffenesRaumschiff.huelleInProzent = getroffenesRaumschiff.huelleInProzent - 50;
			getroffenesRaumschiff.energieversorgungInProzent = getroffenesRaumschiff.energieversorgungInProzent - 50;
			
			if(getroffenesRaumschiff.huelleInProzent <= 0)
			{
				nachrichtAnAlle("Die Lebenserhaltungssysteme von "+ getroffenesRaumschiff.schiffsname + " wurden vollst�ndig zerst�rt.");
				getroffenesRaumschiff.lebenserhaltungssystemInProzent = 0;
				
			}
		}
		
	}
		
	/**
	 *Methode, die eine Nachricht an alle sendet 	
	 * @param message ist die Nachricht die an alle ausgegeben wird
	 */
	
	public void nachrichtAnAlle (String message)
	{
		System.out.println("Nachricht von " + schiffsname +" an Alle:" + message);
		broadcastKummonikator.add(message);
		
	}
	
	/**
	 *Methode, die die Eintr�ge vom Logbuch zur�ckgibt
	 * @return gibt alle Nachrichten zur�ck, die in der Liste gespeichert wurden
	 */
	
	public ArrayList<String> eintraegeLogbuchZurueckgeben()
	{
		return broadcastKummonikator;
	}
	
	
}


