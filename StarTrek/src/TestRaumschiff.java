
public class TestRaumschiff {
	
/**
 * Test-Klasse um Methoden zu �berpr�fen und um das Szenario der Aufgabe nachzustellen
 */
	
	public static void main(String[] args)
	{
		
	/**
	 *neue Raumschiff Objekte erstellen (Klingonen, Romulaner und Vulkanier) 	
	 */
	
	Raumschiff klingonenRaumschiff = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
	Raumschiff romulanerRaumschiff = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
	Raumschiff vulkanierRaumschiff = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
	
	/**
	 *neue Ladung Objekte erstellen (Ferengi Schneckensaft, Borg-Schrott, Rote Materie, Forschungssonde, Bat'leth Klingonen Schwert, Plasma-Waffe, Photonentorpedo)
	 */
	
	Ladung ladung1 = new Ladung("Ferengi Schneckensaft", 200);
	Ladung ladung2 = new Ladung("Borg-Schrott", 5);
	Ladung ladung3 = new Ladung("Rote Materie", 2);
	Ladung ladung4 = new Ladung("Forschungssonde", 35);
	Ladung ladung5 = new Ladung("Bat'leth Klingonen Schwert", 200);
	Ladung ladung6 = new Ladung("Plasma-Waffe", 50);
	Ladung ladung7 = new Ladung("Photonentorpedo", 3);
	
	/**
	 *Ladung im Ladungsverzeichnisses des Klingonen Schiffes adden (Ferengi Schneckensaft, Bat'leth Klingonen Schwert)
	 */
	
	klingonenRaumschiff.addLadung(ladung1);
	klingonenRaumschiff.addLadung(ladung5);

	/**
	 *Ladung im Ladungsverzeichnisses des Remulaner Schiffes adden (Borg-Schrott, Rote Materie, Plasma-Waffe) 
	 */
	
	romulanerRaumschiff.addLadung(ladung2);
	romulanerRaumschiff.addLadung(ladung3);
	romulanerRaumschiff.addLadung(ladung6);
	/**
	 *Ladung im Ladungsverzeichnisses des Vulkanier Schiffes adden ( Forschungssonde, Photonentorpedo)
	 */
	
	vulkanierRaumschiff.addLadung(ladung4);
	vulkanierRaumschiff.addLadung(ladung7);
	
	/**
	 *Die Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner
	 */
	
	klingonenRaumschiff.photonentorpedoSchiessen(romulanerRaumschiff);
	
	/**
	 *Die Romulaner schie�en mit der Phaserkanone zur�ck 
	 */
	
	romulanerRaumschiff.phaserKanoneSchiessen(klingonenRaumschiff);
	
	/**
	 *Die Vulkanier senden eine Nachricht an Alle �Gewalt ist nicht logisch� 
	 */
	
	vulkanierRaumschiff.nachrichtAnAlle("Gewalt ist nicht logisch.");
	
	/**
	 *Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus 
	 */
	
	klingonenRaumschiff.zustandRaumschiff();
	klingonenRaumschiff.aktuelleLadung();
	
	/**
	 *Die Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner
	 */
	
	klingonenRaumschiff.photonentorpedoSchiessen(romulanerRaumschiff);
	klingonenRaumschiff.photonentorpedoSchiessen(romulanerRaumschiff);
	
	/**
	 *Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus
	 */
	
	klingonenRaumschiff.zustandRaumschiff();
	klingonenRaumschiff.aktuelleLadung();
	
	/**
	 *Die Romulaner rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus 
	 */
	
	romulanerRaumschiff.zustandRaumschiff();
	romulanerRaumschiff.aktuelleLadung();
	
	/**
	 *Die Vulkanier rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus
	 */
	
	vulkanierRaumschiff.zustandRaumschiff();
	vulkanierRaumschiff.aktuelleLadung();
	
	/**
	 *ZUSATZ:Logbucheintr�ge der einzelnen Raumschiffe abrufen 
	 */
	
	System.out.println("Logbucheintr�ge:" + romulanerRaumschiff.eintraegeLogbuchZurueckgeben());

	}

}
