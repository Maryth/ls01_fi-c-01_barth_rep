
/**
 * 
 * @author Maria Barth
 * @version 1.5
 * @
 *
 */

public class Ladung 
{
	/**
	 *----------------------------------------------------------------------ATTRIBUTE----------------------------------------------------------------------
	 */
	private String bezeichnung;
	private int menge;

	/**
	 *----------------------------------------------------------------------KONSTRUKTOREN----------------------------------------------------------------------
	 *leerer Konstruktor 
	 */
	
	public Ladung() 
	{
		
	}
	
	/**
	 * 
	 * @param bezeichnung gibt den namen der Ladung an
	 * @param menge gibt die Menge der Ladung an
	 */
	
	public Ladung( String bezeichnung, int menge) 
	{
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	/**
	 *----------------------------------------------------------------------GETTER UND SETTER---------------------------------------------------------------------- 
	 */
	
	/**
	 *Bezeichnung _____________________________________ 
	 *Setzt die Bezeichnung der Ladung
	 * @param bezeichnung bestimmt die Bezeichnung der Ladung
	 */
	
	public void setBezeichnung ( String bezeichnung)
	{
		this.bezeichnung = bezeichnung;
	}
	
	/**
	 * Liefert die Bezeichnung der Ladung
	 * @return gibt die die Bezeichnung der Ladung als Datentyp String wieder
	 */
	
	public String getBezeichnung ()
	{
		return bezeichnung;
	}
	
	/**
	 * Menge _____________________________________
	 * Setzt die Menge der Ladung
	 * @param menge setzt die Menge der Ladung
	 */
	
	public void setMenge (int menge)
	{
		this.menge = menge;
	}
	
	/**
	 * Liefer die Menge der Ladung
	 * @return gibt die Menge der Ladung in int wieder
	 */
	
	public int getMenge ()
	{
		return menge;
	}
	
	/**
	 *----------------------------------------------------------------------METHODEN---------------------------------------------------------------------- 
	 */
	
	/**
	 * Methode, um die Bezeichnung und die Menge als String wieder geben zu k�nnen
	 * wird ben�tigt um das Ladungsverzeichnis eines Raumschiffes auzugeben
	 */
	
	public String toString(){
        return   bezeichnung + ", Anzahl: " + menge;
    }
    
}

