
public class Fahrenheit {
	
public static void main(String[] args) {

tabelle();

}

    static void tabelle(){
        double c1= -28.8889;
        double c2= -23.3333;
        double c3= -17.7778;
        double c4= -6.6667;
        double c5= -1.1111;

        System.out.println("Fahrenheit   |   Celsius");
        System.out.println("------------------------");
        System.out.print("-20");
        System.out.printf("%11s","|");
        System.out.printf("%10s\n", runden(c1));
        System.out.printf("-10");
        System.out.printf("%11s","|");
        System.out.printf("%10s\n", runden(c2));
        System.out.printf("0  ");
        System.out.printf("%11s","|");
        System.out.printf("%10s\n", runden(c3));
        System.out.printf("20 ");
        System.out.printf("%11s","|");
        System.out.printf("%10s\n", runden(c4));
        System.out.printf("30 ");
        System.out.printf("%11s","|");
        System.out.printf("%10s\n", runden(c5));
    }

            static double runden(double input) {
            return Math.round(input*100.0) / 100.0;
            }
}



